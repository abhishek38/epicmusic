package com.example.abhishek.epicmusic.PagerFragment.AlbumPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abhishek.epicmusic.MainActivity;
import com.example.abhishek.epicmusic.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Abhishek on 27-01-2018.
 */

public class  AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumViewHolder> {

    private List<AlbumModel> albumlist;
    private Context context;

    public AlbumAdapter(List<AlbumModel> albumlist, Context context) {
        this.albumlist = albumlist;
        this.context = context;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_grid_layout,parent,false);
        return new AlbumViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder holder, int position) {
        AlbumModel model=albumlist.get(position);
        holder.albumImage.setImageBitmap(model.getAlbumImage());

    }

    @Override
    public int getItemCount() {
        return albumlist.size();
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder{

         CircleImageView albumImage;


        public AlbumViewHolder(View itemView) {
            super(itemView);

            albumImage=(CircleImageView) itemView.findViewById(R.id.albumList_art);
           // gridviewText.setTypeface(main.style);
        }


    }
}
