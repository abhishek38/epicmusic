package com.example.abhishek.epicmusic.PagerFragment.AlbumPackage;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abhishek.epicmusic.MainActivity;
import com.example.abhishek.epicmusic.PagerFragment.SongsPackage.SongListAdapter;
import com.example.abhishek.epicmusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlbumFragment extends Fragment {

    RecyclerView recyclerView;
    AlbumAdapter adapter;
    List<AlbumModel> list;

    public AlbumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_album, container, false);
        recyclerView = v.findViewById(R.id.album_recycler);
        int colspan = 2;
        for (int i = 0; i < colspan; i++) {

            list = new ArrayList<>();


        }
        getAlbum();

        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), colspan));


        adapter = new AlbumAdapter(list, getContext());
        recyclerView.setAdapter(adapter);
        // Inflate the layout for this fragment
        return v;


    }

    public List getAlbum() {
        Context applicationContext = MainActivity.getContextOfApplication();
        ContentResolver resolver = applicationContext.getContentResolver();
        Uri AlbumUri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        Cursor albumCursor = resolver.query(AlbumUri, null, null, null, null);

        if (albumCursor != null && albumCursor.moveToFirst()) {
            int albumart = albumCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);

            do {
                String currentAlbum = albumCursor.getString(albumart);

                Bitmap albm = BitmapFactory.decodeFile(currentAlbum);

                AlbumModel model = new AlbumModel(albm);
                list.add(model);
            }
            while (albumCursor.moveToNext());
        }
        return list;
    }


}
