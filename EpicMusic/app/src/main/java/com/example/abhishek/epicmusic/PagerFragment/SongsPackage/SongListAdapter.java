package com.example.abhishek.epicmusic.PagerFragment.SongsPackage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abhishek.epicmusic.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Abhishek on 23-01-2018.
 */

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.songsViewHolder> {

    private List<SongsModel> list;
    private Context context;

    public SongListAdapter(List<SongsModel> list, Context context) {
       this.list=list;
        this.context = context;
    }

    @Override
    public songsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.songrow_format,parent,false);
        return new songsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(songsViewHolder holder, int position) {
        SongsModel model=list.get(position);
        holder.Songname.setText(model.getSongname());
        holder.Artistname.setText(model.getArtistname());
        // TODO: check the image datatype
        holder.Albumbart.setImageBitmap(model.getAlbumbart());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class songsViewHolder extends RecyclerView.ViewHolder{

        public TextView Songname,Artistname;
        public CircleImageView Albumbart;

        public songsViewHolder(View itemView) {
            super(itemView);

            Songname=(TextView)itemView.findViewById(R.id.song_name);
            Artistname=(TextView)itemView.findViewById(R.id.artist_name);
            Albumbart=(CircleImageView)itemView.findViewById(R.id.albumb_art);
        }
    }
}
