package com.example.abhishek.epicmusic.PagerFragment.SongsPackage;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abhishek.epicmusic.MainActivity;
import com.example.abhishek.epicmusic.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SongsFragment extends Fragment {

    public RecyclerView recyclerView;
    public SongListAdapter adapter;
    private List<SongsModel> list;


    public SongsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_songs, container, false);
        recyclerView = v.findViewById(R.id.songslist_Recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        list = new ArrayList<>();

        getMusic();

        adapter = new SongListAdapter(list, getContext());
        recyclerView.setAdapter(adapter);


        // Inflate the layout for this fragment
        return v;
    }

    public List getMusic() {
        Context applicationContext = MainActivity.getContextOfApplication();
        ContentResolver resolver = applicationContext.getContentResolver();
        Uri SongUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri AlbumUri=MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        Cursor songcursor = resolver.query(SongUri, null, null, null, null);
        Cursor AlbumCursor=resolver.query(AlbumUri,null,null,null,null);


        if (songcursor != null && songcursor.moveToFirst()&&AlbumCursor!=null&&AlbumCursor.moveToFirst()) {
            int SongName = songcursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int ArtistName = songcursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int AlbumArt=AlbumCursor.getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART);


            do {
                String currentTitle = songcursor.getString(SongName);
                String currentArtist = songcursor.getString(ArtistName);
                String currentAlbumArt=AlbumCursor.getString(AlbumArt);

                Bitmap bm= BitmapFactory.decodeFile(currentAlbumArt);


                SongsModel model = new SongsModel(currentTitle, currentArtist,bm);
                list.add(model);


            }
            while (songcursor.moveToNext()&&AlbumCursor.moveToNext());
        }
        return list;
    }


}
