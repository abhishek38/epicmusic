package com.example.abhishek.epicmusic;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abhishek.epicmusic.PagerFragment.AlbumPackage.AlbumFragment;
import com.example.abhishek.epicmusic.PagerFragment.PlaylistPackage.PlaylistFragment;
import com.example.abhishek.epicmusic.PagerFragment.SongsPackage.SongsFragment;

public class MainActivity extends FragmentActivity {
    private  static final int My_Permission_Request=1;
    TextView title;
   public Typeface style;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},My_Permission_Request);
            } else
            {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},My_Permission_Request);
            }
        }

        contextOfApplication = getApplicationContext();

        title=(TextView)findViewById(R.id.appTitle);
         style=Typeface.createFromAsset(getApplicationContext().getAssets() ,"android_7.ttf");
        title.setTypeface(style);

        ViewPager pager=(ViewPager)findViewById(R.id.mainscreenPager);
        FragmentManager manager=getSupportFragmentManager();
        pager.setAdapter(new PagerAdapter(manager));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permission, int[] grantResults) {
        switch (requestCode){
            case My_Permission_Request: {
                if(grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED)
                        Toast.makeText(contextOfApplication, "Permission granted", Toast.LENGTH_SHORT).show();
                } else{
                    Toast.makeText(contextOfApplication, "NO Permission", Toast.LENGTH_SHORT).show();
                }
            }


        }

    }

    public static Context contextOfApplication;
    public static Context getContextOfApplication()
    {
        return contextOfApplication;
    }

    class PagerAdapter extends FragmentStatePagerAdapter{

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment=null;
            if(position==0){
                fragment=new SongsFragment();

            }
            if(position==1){
                fragment=new AlbumFragment();

            }
            if(position==2){
                fragment=new PlaylistFragment();

            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position==0){
                return "Songs";
            }
            if(position==1){
                return "Albums";
            }
            if(position==2){
                return "Playlists";
            }
           return null;
        }
    }
}
