package com.example.abhishek.epicmusic.PagerFragment.SongsPackage;

import android.graphics.Bitmap;

/**
 * Created by Abhishek on 23-01-2018.
 */

public class SongsModel {
    private String songname;
    private String artistname;
    private Bitmap albumbart;

    SongsModel(String songname, String artistname,Bitmap albumbart) {
        this.songname = songname;
        this.artistname = artistname;
        this.albumbart=albumbart;
    }

    public String getSongname() {
        return songname;
    }

    public String getArtistname() {
        return artistname;
    }

    public Bitmap getAlbumbart() {
        return albumbart;
    }
}
