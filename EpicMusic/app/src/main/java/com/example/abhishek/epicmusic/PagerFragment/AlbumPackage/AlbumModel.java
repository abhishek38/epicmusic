package com.example.abhishek.epicmusic.PagerFragment.AlbumPackage;

import android.graphics.Bitmap;

/**
 * Created by Abhishek on 27-01-2018.
 */

public class AlbumModel {
    Bitmap AlbumImage;

    public Bitmap getAlbumImage() {
        return AlbumImage;
    }

    public AlbumModel(Bitmap albumImage) {
        AlbumImage = albumImage;

    }
}
